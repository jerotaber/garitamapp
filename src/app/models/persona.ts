export interface Persona {
     id: number;
     nombre: string;
     apellido: string;
     documento_numero: number;
     documento_tipo: string;
}