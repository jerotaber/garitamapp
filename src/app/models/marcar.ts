import { Persona } from './persona';
import { Ubicacion } from './ubicacion';

export interface Marcar {
     persona: Persona;
     status: boolean;
     ubicacion?: Ubicacion;
     ingreso?: number;
     error?: string;
     expanded: Boolean;
}