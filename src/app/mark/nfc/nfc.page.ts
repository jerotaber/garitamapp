import { Component, OnInit } from '@angular/core';
import { NFC, Ndef } from '@ionic-native/nfc/ngx';
@Component({
  selector: 'app-nfc',
  templateUrl: './nfc.page.html',
  styleUrls: ['./nfc.page.scss'],
})
export class NfcPage implements OnInit {
  myListener: any;
  nfcData: string;
  constructor(private nfc: NFC, private ndef: Ndef) { 
    //Inicia check de dispositivo y agrega un listener para NFC
    this.nfc.enabled().then((resolve) => {
      this.addListenNFC();
    }).catch((reject) => {
      alert("NFC is not supported by your Device");
    });
  }

  ngOnInit() {
  }
  addListenNFC() {
    this.myListener = this.nfc.addTagDiscoveredListener().subscribe(data => {
      //window.removeEventListener; //this is not working.
      if (data) {
        this.nfcData = JSON.stringify(data);
      }
      else {
        alert('NFC_NOT_DETECTED');
      }
    });
  }


}
