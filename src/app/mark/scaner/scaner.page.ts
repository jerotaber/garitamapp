import { Component, OnInit, HostListener, ViewChild, ElementRef } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { Keyboard } from '@ionic-native/keyboard/ngx';
@Component({
  selector: 'app-scaner',
  templateUrl: './scaner.page.html',
  styleUrls: ['./scaner.page.scss'],
})
export class ScanerPage implements OnInit {
  mensaje: string;
  @ViewChild("dni", { static: false }) focusInput: ElementRef;
  constructor(private _sanitizer: DomSanitizer, private keyboard: Keyboard) { 

    // Hide Keyboard
    
    //
  }
  inputblur() {
    this.focusInput.nativeElement.focus();
  }
  ngOnInit() {
    this.keyboard.onKeyboardWillShow().subscribe(() => {
      console.log('show');
      this.keyboard.hide();
    });
  }

  onSend() {
    this.mensaje = (<HTMLInputElement>document.getElementById('dniString')).value;
    (<HTMLInputElement>document.getElementById('dniString')).value = "";
  }
  @HostListener('document:keypress', ['$event'])
  handleKeyboardEvent(event: KeyboardEvent) {
    if (event.keyCode == 13) {
      this.mensaje = (<HTMLInputElement>document.getElementById('dniString')).value;
      (<HTMLInputElement>document.getElementById('dniString')).value = "";
    }
  }
}
