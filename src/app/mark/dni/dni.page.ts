import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NetworkCheckService } from 'src/app/services/network-check.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MarkService } from "../../services/mark.service";
import { Marcar } from 'src/app/models/marcar';
import { LoadingController } from '@ionic/angular';
import { AlertService } from 'src/app/services/alert.service';
@Component({
  selector: 'app-dni',
  templateUrl: './dni.page.html',
  styleUrls: ['./dni.page.scss'],
})
export class DniPage implements OnInit {
  public ingressForm: FormGroup;
  private log: Marcar[];

  constructor(private alertService: AlertService ,private router: Router, private net: NetworkCheckService, public formBuilder: FormBuilder, public ingressService: MarkService, public loadingCtrl: LoadingController) {
    this.ingressForm = this.formBuilder.group({
      dniString: ['', [Validators.required, Validators.maxLength(50)]]
    });
}

  ngOnInit() {
    this.net.check();
  }

  back() {
    this.router.navigateByUrl('/home');
  }
  /////////////SEND POST TO SERVER//////////////
  async sendIngress() {
    const loading = await this.loadingCtrl.create({
      message: 'Verificando'
    });
    loading.present();
    const dniString = this.ingressForm.get(['dniString']).value;
    this.ingressService.sendPost(dniString).
      subscribe(responseData => {
        console.log({ ...responseData });
        setTimeout(() => {
          loading.dismiss();
          this.pushArray(responseData);
          this.alertService.AlertSuccess(responseData, 'El usuario se ingreso con exito');
          this.router.navigateByUrl('/home');
        }, 1000);
      },
        (error) => {
          setTimeout(() => {
            loading.dismiss();
            this.pushArray(error.error);
            this.alertService.AlertFailed(error.error, 'Hubo un problema al ingresar al usuario');
            this.router.navigateByUrl('/home');

          }, 1000);
        });
  }

  /////////PUSH NEW ITEM IN ARRAY////////////
  pushArray(responseData) {
    console.log({ ...this.log });
    console.log(this.log);

    this.log = JSON.parse(localStorage.getItem('log'));
    if (this.log != null && this.log.length > 9) {
      this.log.splice(0, 1);
    }
    if (this.log == null) {
      this.log = [];
    }
    this.log.push(responseData);
    localStorage.setItem('log', JSON.stringify(this.log));
    console.log({ ...this.log });
    console.log(this.log);


  }
}
