import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './guards/auth.guard';

const routes: Routes = [
  { path: '', redirectTo: 'login', pathMatch: 'full' },
  {
    path: 'login',
    canActivate: [],
    loadChildren: () => import('./login/login.module').then(m => m.LoginPageModule)
  },
  {
    path: 'home',
    canActivate: [AuthGuard],
    loadChildren: () => import('./home/home.module').then(m => m.HomePageModule)
  },
  {
    path: 'no-network',
    loadChildren: () => import('./no-network/no-network.module').then( m => m.NoNetworkPageModule)
  },
  {
    path: 'dni',
    canActivate: [AuthGuard],
    loadChildren: () => import('./mark/dni/dni.module').then( m => m.DniPageModule)
  },
  {
    path: 'scaner',
    canActivate: [AuthGuard],
    loadChildren: () => import('./mark/scaner/scaner.module').then( m => m.ScanerPageModule)
  },
  {
    path: 'nfc',
    canActivate: [AuthGuard],
    loadChildren: () => import('./mark/nfc/nfc.module').then( m => m.NfcPageModule)
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
