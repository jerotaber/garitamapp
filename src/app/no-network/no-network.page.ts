import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-no-network',
  templateUrl: './no-network.page.html',
  styleUrls: ['./no-network.page.scss'],
})
export class NoNetworkPage implements OnInit {

  constructor(private router: Router) { }
  url: string;
  ngOnInit() {
    if (localStorage.getItem('token')) {
      this.url = 'home';
    }
    else {
      this.url = 'login';
    }
  }

  loginPage() {
    this.router.navigateByUrl('/' + this.url);
  }
}