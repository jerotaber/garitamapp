import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'reverse' })

export class ReversePipe implements PipeTransform {
    //Este PIPE invierte el orden del array para mostrar primero los ultimos pepidos al servidor
    transform(value) {
        if (value)
            return value.slice().reverse();
        else
            return null;
    }
}