import { Injectable } from '@angular/core';
import { AlertController } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class AlertService {

  constructor(private alertCtrl: AlertController) { }
  async AlertFailed(message, header) {
    const alert = await this.alertCtrl.create({
      subHeader: header,
      message: JSON.stringify(message.error),
      buttons: ['OK']
    });
    await alert.present();
  }
  async AlertSuccess(message, header) {
    const alert = await this.alertCtrl.create({
      subHeader: header,
      message: JSON.stringify(message.error),
      buttons: ['OK']
    });
    await alert.present();
  }
  
}
