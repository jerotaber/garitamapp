import { Platform } from '@ionic/angular';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { catchError, timeout } from 'rxjs/operators';
import { throwError, BehaviorSubject, Subject } from 'rxjs';
import { Storage } from '@ionic/storage';
import { Usuario } from '../interfaces';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {
  private urlAPI = 'https://mapp.com.ar/test/api/garita/login';
  authenticationState = new BehaviorSubject(false);
  
  constructor(private storage: Storage, private plt: Platform, private httpClient: HttpClient) {
  }


  private miSubject = new Subject();
  public miObservable$ = this.miSubject.asObservable();

  private usuario:Usuario={};
 
 
 
  get() {
    return this.usuario;
  }

  autenticate(user, password) {
    var form = new FormData();
    form.append("user", user);
    form.append("password", password);
    return this.httpClient.post(this.urlAPI, form).pipe(
      timeout(10000),
      catchError(err => {
        console.log('Error');
        return throwError(err);
      })
    );
  }

  setSession(authResult) {
    localStorage.setItem('token', authResult.token);
    this.usuario=={
        id:1,
        barrio_id:12,
        nombre:"Pablo",
        apellido:"Aguiar",
        razon_social:"Garita 1",
      };
    this.miSubject.next();
    this.authenticationState.next(true);
    console.log(localStorage.getItem('token'));
  }
  logout() {
    localStorage.removeItem("token");
    this.authenticationState.next(false);
  }
  public isLoggedIn() {
    return this.authenticationState.value;
  }

}