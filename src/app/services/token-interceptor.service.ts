import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler } from '@angular/common/http';
import { AuthenticationService } from './authentication.service';

@Injectable({
  providedIn: 'root'
})
export class TokenInterceptorService implements HttpInterceptor {

  constructor( public auth: AuthenticationService) { }
  intercept(req: HttpRequest<any>, next: HttpHandler) {
    if(this.auth.isLoggedIn()){
    const modifiedRequest = req.clone({headers: req.headers.append('token',localStorage.getItem('token'))});
    return next.handle(modifiedRequest);
    }
    else{
      return next.handle(req);
    }
  }
}
  