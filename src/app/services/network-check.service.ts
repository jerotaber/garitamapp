import { Injectable, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Network } from '@ionic-native/network/ngx';

@Injectable({
  providedIn: 'root'
})
export class NetworkCheckService implements OnInit{

  constructor(private router: Router, private network: Network) { 
  }
  ngOnInit() {}

  check () {
    this.network.onDisconnect().subscribe(() => {
      this.router.navigateByUrl('/no-network');
    });
    
    var networkState = this.network.type;
    console.log(networkState);

    if (networkState == 'none') {
      this.router.navigateByUrl('/no-network');
    }
  }
}
