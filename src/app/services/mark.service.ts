import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { throwError, Observable } from 'rxjs';
import { Marcar } from '../models/marcar';

@Injectable({
  providedIn: 'root'
})
export class MarkService {
  private urlAPI = 'https://mapp.com.ar/api/garita/marcar';

  constructor(private httpClient: HttpClient) { }

  sendPost(data): Observable<Marcar> {
    var form = new FormData();
    form.append("persona", data);
    form.append("lector", "Manual");

    return this.httpClient.post<Marcar>(this.urlAPI, form).pipe(
      //map(res => res.marcar || []),
      catchError(err => {
        console.log('Error');
        return throwError(err);
      })
    );
  }
}
