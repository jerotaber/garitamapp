import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Network } from '@ionic-native/network/ngx';
import { LoadingController } from '@ionic/angular';
import { AlertService } from '../services/alert.service';
import { AuthenticationService } from '../services/authentication.service';
import { NetworkCheckService } from '../services/network-check.service';
@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  error: boolean=false;
  public loginForm: FormGroup;
  constructor(
    public loadingCtrl: LoadingController,
    private authService: AuthenticationService,
    private alertService: AlertService,
    public formBuilder: FormBuilder,
    private net: NetworkCheckService
  ) {
    this.loginForm = this.formBuilder.group({
      username: ['', [Validators.required, Validators.minLength(4), Validators.maxLength(14)]],
      password: ['', [Validators.required, Validators.minLength(4), Validators.maxLength(14)]]
    });
  }

  ngOnInit() {
    this.net.check();
  }

  async verificarUsuario() {
    const loading = await this.loadingCtrl.create({
      message: 'Verificando'
    });
    loading.present();
    this.authService.autenticate(
      this.loginForm.get(['username']).value,
      this.loginForm.get(['password']).value
    ).subscribe(responseData => {
      console.log(responseData);
      setTimeout(() => {
        loading.dismiss();
        this.authService.setSession(responseData)
      }, 1000);
    },
      (error) => {
        console.log(error);
        setTimeout(() => {
          loading.dismiss();
          this.error=true;
        }, 1000);
      });
  }
}
