
/**
 * Usuario garita logeado
 */
export interface Usuario {
    id?:number;
    username?:string;
    password?:string;
    nombre?:string;
    apellido?:string;
    razon_social?:string;
    //barrio de la garita
    barrio_id?:number;

}