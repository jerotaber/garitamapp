import { Component } from '@angular/core';

import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { Router } from '@angular/router';
import { AuthenticationService } from './services/authentication.service';
import { Storage } from '@ionic/storage';
import { Subscription } from 'rxjs';
import { Usuario } from './interfaces';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent {
  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private authenticationService: AuthenticationService,
    private router: Router,
    private navController: NavController
    
  ) {
    this.initializeApp();
  }
  //Usuario loggeado
  public usuario:Usuario=null;
  //Suscripcion al servicio de datos
  public miSuscripcion: Subscription;

  ngOnInit(){

    this.miSuscripcion = this.authenticationService.miObservable$.subscribe(()=>{
      this.usuario=this.authenticationService.get();
      console.log("Usuario Actualizado");
    });
    this.navController.navigateRoot(['/login']); 
  }
  logout(){
    //Borra los datdos del usuario
    this.usuario=null;
    this.navController.navigateRoot(['/login']); 
  }
  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
      this.authenticationService.authenticationState.subscribe(state => {
        if (state) {
          this.router.navigate(['home']);
        } else {
          this.router.navigate(['login']);
        }
      });
    });
  }
}