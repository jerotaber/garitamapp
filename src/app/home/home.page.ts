import { Component, OnInit, OnChanges } from '@angular/core';
import { AuthenticationService } from '../services/authentication.service';
import { Router } from '@angular/router';
import { Network } from '@ionic-native/network/ngx';
import { NetworkCheckService } from '../services/network-check.service';
import { Marcar } from '../models/marcar';
@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit{
  private log: Marcar[];
  //private token: string = localStorage.getItem('token');
  constructor(private authService: AuthenticationService, private router: Router, private net: NetworkCheckService) { }

  ngOnInit() {
    this.net.check();
    this.log = JSON.parse(localStorage.getItem('log'));
    this.router.events.subscribe((val) => {
      this.log = JSON.parse(localStorage.getItem('log'));
    })
  }

  logout() {
    this.authService.logout();
  }

  dniPage() {
    this.router.navigateByUrl('/dni');
  }
  scanerPage() {
    this.router.navigateByUrl('/scaner');
  }
  nfcPage() {
    this.router.navigateByUrl('/nfc');
  }
}