GaritaMAPP

## Instalaciones Necesarias

Para correr correctamente la app, es necesario la instalacion de las siguientes herramientas:

1. Apache Cordova [Link](https://cordova.apache.org/#getstarted).
2. JDK y Android SDK [Link](https://cordova.apache.org/docs/es/latest/guide/platforms/android/).
3. Node JS [Link](https://nodejs.org/es/).
4. Ionic cli [Link](https://ionicframework.com/docs/installation/cli).
5. Cordova [Link](https://ionicframework.com/docs/v3/intro/installation/).

---

## Creacion de un nuevo proyecto ionic-cordova

Para crear un nuevo proyecto ionic-cordova en android, se deben correer los siguientes comandos en la terminal:
---
```
ionic start [name] [type]
```
---
```
ionic cordova prepare android
```
---
Para crear la apk de este proyecto se utiliza el comando:
---
```
ionic cordova build android
```
---

## Plugins necesarios para este proyecto
-Phonegap-nfc [Link](https://github.com/apache/cordova-plugin-network-information).
[Guia instalacion ionic](https://ionicframework.com/docs/native/network).

-Camera [Link](https://github.com/apache/cordova-plugin-camera).
[Guia instalacion ionic](https://ionicframework.com/docs/native/camera).

-Keyboard [Link]( https://github.com/ionic-team/cordova-plugin-ionic-keyboard).
[Guia instalacion ionic](https://ionicframework.com/docs/native/keyboard).

-Storage [Link]( https://github.com/TheCocoaProject/cordova-plugin-nativestorage).
[Guia instalacion ionic](https://ionicframework.com/docs/native/native-storage).

-Angular materials
---
Instalacion
Network
```
ionic cordova plugin add cordova-plugin-network-information
npm install @ionic-native/network
```
Phonegap
```
ionic cordova plugin add phonegap-nfc
npm install @ionic-native/nfc
```
Camera
```
ionic cordova plugin add cordova-plugin-camera
npm install @ionic-native/camera
```
Keyboard
```
ionic cordova plugin add cordova-plugin-ionic-keyboard
npm install @ionic-native/keyboard
```
Storage
```
ionic cordova plugin add cordova-plugin-nativestorage
npm install @ionic-native/native-storage
```
Materials
```
ng add @angular/material
```

## Caso de uso
una vez descargado el proyecto se puede correr en el browser con el codigo:
```
ionic serve
```
O crear una apk con:
```
ionic cordova build android
```
---
La apk se enciuentra en la carpeta:

../platforms/android/app/build/outputs/apk/debug

El archivo por default es:
app-debug.apk

Dicho archivo debe ser transferido al mobile e instalado
Una vez instalado, simplemente debe abrirse, automaticamente reconocera tarjetas nfc si son colocadas en el lector correspondiente.


